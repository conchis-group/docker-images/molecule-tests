FROM docker:latest

ARG ANSIBLE_VERSION=2.16.2
ARG MOLECULE_VERSION=6.0.3
ARG YAMLLINT_VERSION=1.33.0
ARG ANSIBLELINT_VERSION=6.22.2

RUN echo "http://dl-cdn.alpinelinux.org/alpine/latest-stable/community" >> /etc/apk/repositories 

RUN apk add --update python3 python3-dev py3-pip  py3-cryptography curl openssl ca-certificates bash git zip openssh-client rsync && \
    apk add --no-cache --virtual build-dependencies linux-headers build-base python3-dev libffi-dev openssl-dev py-psutil 

RUN pip install --no-cache-dir --upgrade --break-system-packages pip setuptools && \
    pip install --upgrade --break-system-packages 'molecule-plugins[docker]' dnspython \
	ansible \
	molecule~=${MOLECULE_VERSION} \
	yamllint~=${YAMLLINT_VERSION} \
	ansible-lint~=${ANSIBLELINT_VERSION} 

RUN apk del build-dependencies && \
    rm -rf /var/cache/apk/* && \
    rm -r /root/.cache 

WORKDIR /role

