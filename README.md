# molecule-tests

[Docker Hub](https://hub.docker.com/repository/docker/conchism/molecule-tests/general)

## Usage
The image is for running ansible-lint, yamllint, molecule test. The image is based on docker:latest.

## Examples

For using ansible-lint:

```
.molecule-ansible-linting:
  stage: lint
  image: "conchism/molecule-tests:1.0.5"
  before_script:
    - ansible-lint --version
  script:
    - ansible-lint
```

For using yamllint:

```
.molecule-yaml-linting:
  stage: lint
  image: "conchism/molecule-tests:1.0.5"
  before_script:
    - yamllint --version
  script:
    - yamllint .
```

For using molecule:

```
.molecule-test:
  stage: test
  image: "conchism/molecule-tests:1.0.5"
  services:
    - docker:24.0.5-dind
  script:
    - molecule test -s default
```
